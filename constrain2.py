size = int(input("enter the number:"))
matrix = []
list_of_numbers = []

for numbers in range(1 , size + 1):
        list_of_numbers.append(numbers)
print(list_of_numbers)
temp_list = list_of_numbers

#assigning the list of numbers to every element of the matrix


def fill():
        for row in range(size):
                temp = []
                for col in range(size):
                        temp.append(0)
                matrix.append(temp)
        return matrix

matrix = fill()

#giving random inputs
no_of_values = int(input("how many values you want to give :"))
def given_values():
        for i in range(no_of_values):
            row = int(input("which row :"))
            col = int(input("which col :"))
            matrix[row][col] = int(input("enter the values :"))
        return matrix
matrix = given_values()

#checking the possibilities and eliminating the unwanted values  


def columns():
        temp_list = []
        for row in range(size):
                temp_list.extend(list_of_numbers)
                for col in range(size):                        
                        if matrix[row][col]  in temp_list :
                                temp_list.remove(matrix[row][col])
                       
                for col in range(size):
                        if type(matrix[row][col]) == list:
                                matrix[row][col] = []
                                matrix[row][col].extend(temp_list)                                              
                temp_list = []                    
        return matrix                 


def rows() :   
        matrix = columns()
        list_given_values = []
        for col in range(size):
                for row in range(size):             
                        if type(matrix[row][col]) != list :
                                list_given_values.append(matrix[row][col])  
                for row in range(size):
                        if type(matrix[row][col]) == list :
                                for i in list_given_values:
                                        if i  not in matrix[row][col]:
                                                continue 
                                        else : matrix[row][col].remove(i)                                                     
                list_given_values = []
        return matrix


def pre_matrix():
    for row in range(size):
        for col in range(size):
            if type(matrix[row][col]) is list and len(matrix[row][col]) == 1 :
                matrix[row][col] = matrix[row][col][0]
    return matrix
        

def count_req():
    count = 0
    for row in range(size):
        for col in range(size):
            if type(matrix[row][col]) is list and len(matrix[row][col]) > 1:
                count += 1
    return count
#final matrix
   

def req_matrix():
    count = count_req()
    while True:
        if count > 0:
            matrix = columns()
            matrix = rows()       
            matrix = pre_matrix()
            count = count_req()              
        elif count == 0:
            break
    return matrix
matrix = req_matrix()
for element in matrix:
    print(*element)    



